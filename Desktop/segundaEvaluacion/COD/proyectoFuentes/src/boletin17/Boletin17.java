/*
Realiza un programa que cree un array chamado “numeros” con 6 numeros aleatorios dun rango de 1 a 50 .
Seguidamente visualiza o array o revés ( 0 primeiro elemento visualizaráse  o último e asi sucesivamente )
 */

package boletin17;

/**
 *
 * @author findag
 */
public class Boletin17 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        int [] numeros = new int[6];
        Boletin17 obx = new Boletin17();
        obx.aleatorio(numeros);
        obx.visualiza(numeros);
        
    }
    public void aleatorio(int [] nume)
    {
        for (int i=0; i < nume.length; i++)
        {
            nume[i]= (int) Math.round((Math.random()*51+1));
        }
    }
    public void visualiza(int [] nume)
    {
        for (int i=nume.length - 1; i>=0; i--)
        {
            System.out.println(nume[i]);
        }
    }
    
}
